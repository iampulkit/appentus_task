package common.example.appentus_task;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.UserAdapterVH> {

    private List<DataModel> dataModelList;
    private Context context;
    private ClickedItem clickedItem;

    public ImageAdapter(ClickedItem clickedItem) {
        this.clickedItem = clickedItem;
    }

    public void setData(List<DataModel> dataModelList) {
        this.dataModelList = dataModelList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public UserAdapterVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new ImageAdapter.UserAdapterVH(LayoutInflater.from(context).inflate(R.layout.items,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull UserAdapterVH holder, int position) {

        DataModel dataModel = dataModelList.get(position);

        String url = dataModel.getDownload_url();

        Glide.with(this.context)
                .load(url)
                .into(holder.imageView);

    }

    public interface ClickedItem{
        public void ClickedUser(DataModel dataModel);
    }

    @Override
    public int getItemCount() {
        return dataModelList.size();
    }

    public class UserAdapterVH extends RecyclerView.ViewHolder {

        ImageView imageView;

        public UserAdapterVH(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_item);


        }
    }
}