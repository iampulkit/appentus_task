package common.example.appentus_task;

import java.io.Serializable;

public class DataModel implements Serializable {
    private String id;
    private String author;
    private int width;
    private int height;
    private String url;
    private String download_url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDownload_url() {
        return download_url;
    }

    public void setDownload_url(String download_url) {
        this.download_url = download_url;
    }
    @Override
    public String toString() {
        return "UserResponse{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", download_url='" + download_url + '\'' +
                ", height='" + height + '\'' +
                ", width='" + width + '\'' +
                ", author='" + author + '\'' +
                '}';
    }
}
