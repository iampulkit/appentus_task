package common.example.appentus_task;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Services {
    @GET("list/")
    Call<List<DataModel>> getData();
}
