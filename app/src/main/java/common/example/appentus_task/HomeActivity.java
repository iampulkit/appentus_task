package common.example.appentus_task;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements ImageAdapter.ClickedItem {

    RecyclerView recyclerView;

    ImageAdapter imageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        recyclerView = findViewById(R.id.rv_images);


        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));


        imageAdapter = new ImageAdapter(this::ClickedUser);

        getData();
    }
    public void getData(){

        Call<List<DataModel>> userlist = ApiClient.getUserService().getData();

        userlist.enqueue(new Callback<List<DataModel>>() {
            @Override
            public void onResponse(Call<List<DataModel>> call, Response<List<DataModel>> response) {

                if(response.isSuccessful()){
                    List<DataModel> dataModels = response.body();
                    imageAdapter.setData(dataModels);
                    recyclerView.setAdapter(imageAdapter);

                }

            }

            @Override
            public void onFailure(Call<List<DataModel>> call, Throwable t) {
                Log.e("failure",t.getLocalizedMessage());

            }
        });
    }

    @Override
    public void ClickedUser(DataModel dataModel) {

    }
}